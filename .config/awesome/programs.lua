local scripts = os.getenv("HOME") .. "Git/dotfiles.nix/scripts"
local wallpapers = os.getenv("HOME") .. "Git/dotfiles.nix/images/wallpapers"

return {
  -- List of apps to start by default on some actions
    default = {
        browser = "qutebrowser",
        terminal = "kitty",
        editor = os.getenv("EDITOR") or "nvim",
        editor_cmd = "kitty -e nvim",
        rofi = "rofi -show drun ",
        lock = "betterlockscreen -l",
        filemanager_cmd = "kitty -e ranger"
    },

    -- List of apps to start once on start-up
    run_on_start_up = {
        "picom -b",
        "redshift-gtk",
        "caffeine",
        "mpd",
        scripts .. "/mpd_notify.sh &",
        "udiskie --smart-tray",
        "nitrogen --restore"
    }
}
