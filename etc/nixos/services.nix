
{ config, pkgs, ... }:

{

# List services that you want to enable:

services = {
        # Enable the OpenSSH daemon.
        openssh.enable = true;
        # Enable CUPS to print documents.
        printing.enable = true;
        # Enable the X11 windowing system.
        xserver = {
          enable = true;
          layout = "br,us";
          xkbVariant = "abnt2,intl";
          xkbOptions = "grp:shift_toogle,grp_led:scroll";
          videoDrivers = [ "modesetting" "nvidia" ];
          libinput.enable = true;
          desktopManager.xterm.enable = false;
          displayManager.startx.enable = true;
          windowManager.awesome = {
            enable = true;
            luaModules = with pkgs.luaPackages; [
              luarocks # package manager for lua
              luadbi-mysql # Database abstration layer
            ];
          }; 

        };
        mingetty.helpLine = ''

                [0;34;40m ███    ██ ██ ██   ██  ██████  ███████ 
                [0;34;40m ████   ██ ██  ██ ██  ██    ██ ██      
                [0;34;40m ██ ██  ██ ██   ███   ██    ██ ███████ 
                [0;34;40m ██  ██ ██ ██  ██ ██  ██    ██      ██ 
                [0;34;40m ██   ████ ██ ██   ██  ██████  ███████ 
                [0;37;40m
        '';

      };

      systemd.services.betterlockscreen = {
        enable = true;
        description = "Locks screen when going to sleep/suspend";
        environment = { DISPLAY = ":0"; };
        serviceConfig = {
          User = "seraphybr";
          Type = "simple";
          ExecStart = ''${pkgs.betterlockscreen}/bin/betterlockscreen -l'';
          TimeoutSec = "infinity";
          alias = [ "betterlockscreen@seraphybr.service" ];
        };
        before = [ "sleep.target" "suspend.target" ];
        wantedBy = [ "sleep.target" "suspend.target" ];
      };
    }
